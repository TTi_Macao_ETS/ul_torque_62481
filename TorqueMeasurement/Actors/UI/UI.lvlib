﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="CheckDate Msg.lvclass" Type="LVClass" URL="../UI Messages/CheckDate Msg/CheckDate Msg.lvclass"/>
		<Item Name="CreateCursors Msg.lvclass" Type="LVClass" URL="../UI Messages/CreateCursors Msg/CreateCursors Msg.lvclass"/>
		<Item Name="DisableDragRangeCursors Msg.lvclass" Type="LVClass" URL="../UI Messages/DisableDragRangeCursors Msg/DisableDragRangeCursors Msg.lvclass"/>
		<Item Name="EnableRunButton Msg.lvclass" Type="LVClass" URL="../UI Messages/EnableRunButton Msg/EnableRunButton Msg.lvclass"/>
		<Item Name="FrontPanelMessages Msg.lvclass" Type="LVClass" URL="../UI Messages/FrontPanelMessages Msg/FrontPanelMessages Msg.lvclass"/>
		<Item Name="GenerateTEDSTransducerEvent Msg.lvclass" Type="LVClass" URL="../UI Messages/GenerateTEDSTransducerEvent Msg/GenerateTEDSTransducerEvent Msg.lvclass"/>
		<Item Name="MoveCursorBounds Msg.lvclass" Type="LVClass" URL="../UI Messages/MoveCursorBounds Msg/MoveCursorBounds Msg.lvclass"/>
		<Item Name="SetMenuRings Msg.lvclass" Type="LVClass" URL="../UI Messages/SetMenuRings Msg/SetMenuRings Msg.lvclass"/>
		<Item Name="setTestAndXdcr Msg.lvclass" Type="LVClass" URL="../UI Messages/setTestAndXdcr Msg/setTestAndXdcr Msg.lvclass"/>
		<Item Name="ShowFrontPanel Msg.lvclass" Type="LVClass" URL="../UI Messages/ShowFrontPanel Msg/ShowFrontPanel Msg.lvclass"/>
		<Item Name="UpdateGraphData Msg.lvclass" Type="LVClass" URL="../UI Messages/UpdateGraphData Msg/UpdateGraphData Msg.lvclass"/>
		<Item Name="UpdateResults Msg.lvclass" Type="LVClass" URL="../UI Messages/UpdateResults Msg/UpdateResults Msg.lvclass"/>
		<Item Name="Write Teds Msg.lvclass" Type="LVClass" URL="../UI Messages/Write Teds Msg/Write Teds Msg.lvclass"/>
		<Item Name="Write UI References Msg.lvclass" Type="LVClass" URL="../UI Messages/Write UI References Msg/Write UI References Msg.lvclass"/>
		<Item Name="WriteGraphData Msg.lvclass" Type="LVClass" URL="../UI Messages/WriteGraphData Msg/WriteGraphData Msg.lvclass"/>
		<Item Name="writeTransducerData Msg.lvclass" Type="LVClass" URL="../UI Messages/writeTransducerData Msg/writeTransducerData Msg.lvclass"/>
		<Item Name="WriteTransducerInfoEvent Msg.lvclass" Type="LVClass" URL="../UI Messages/WriteTransducerInfoEvent Msg/WriteTransducerInfoEvent Msg.lvclass"/>
	</Item>
	<Item Name="UI.lvclass" Type="LVClass" URL="../UI/UI.lvclass"/>
</Library>
