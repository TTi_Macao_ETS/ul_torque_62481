TEMPLATE 0,8,0,"Accelerometer v0.9"  
TDL_VERSION_NUMBER 2  //Version 2 refers to the final IEEE 1451.4 version 1.0 TDL specification
ABSTRACT IEEE P1451.4 template ID 0, version 0.9
ABSTRACT ROM not used.
ABSTRACT Updated to TDL version 2 draft
SPACING

//Physical Base Units: (ratio, radian, steradian, meter, kg, sec, Amp, K, mole, candela)
PHYSICAL_UNIT "V/(m/s^2)",  (0,0,0,1,1,-1,-1,0,0,0,1,0)    // equals m*kg/(s*A)
PHYSICAL_UNIT "Hz",        (0,0,0,0,0,-1,0,0,0,0,1,0)     // equals 1/s
PHYSICAL_UNIT "",          (0,0,0,0,0,0,0,0,0,0,1,0)      // dimensionless

%CalDate, "Calibration date", CAL, 16, DATE,"d-mmm-yyyy",""
%Sens@Ref, "Sensitivity @ Fref", CAL, 16, ConRelRes, 100E-6, 0.0001,  "0.000p", "V/(m/s^2)"
%Reffreq,  "Fref",  CAL, 8, ConRelRes, 10.17501895022, 0.015,  "0p","Hz"
%TF_HP_S, "F hp electrical",  CAL, 12, ConRelRes, 0.01, 0.001, "0.000p", "Hz"
%Sign,  "Phase invertion (0: 0 Deg, 1: 180 Deg)",  CAL, 1, UNINT, "0",""
ENUMERATE DirectionEnum,"x","y","z"
%Direction, "Sensitivity direction (x,y,z)",  CAL, 2, DirectionEnum, "e", ""
SPACING
%MeasID, "Measurement location ID",  USR, 9, UNINT,"###0", ""

ALIGN 8  //May not be needed, but left for compatibility with previous version

ENUMERATE ElecSigTypeEnum,"Voltage Sensor","Current Sensor","Resistance Sensor","Bridge Sensor","LVDT Sensor","Potentiometric Voltage Divider Sensor","Pulse Sensor","Voltage Actuator","Current Actuator","Pulse Actuator"
%ElecSigType, "Transducer Electrical Signal Type", ID, 0, ElecSigTypeEnum,"e","" = "Voltage Sensor"

ENUMERATE MapMethEnum, "Linear","Inverse m/(x+b)","Inverse (b+m/x)","Thermocouple","Thermistor","RTD","Bridge"
%MapMeth, "Mapping Method", ID, 0, MapMethEnum,"e","" = "Linear"

ENUMERATE ACDCCouplingEnum,"DC","AC"
%ACDCCoupling, "AC or DC Coupling", ID, 0, ACDCCouplingEnum,"e","" = "AC"

//User data is not explicitly defined in this subtemplate. If the next three bits read
// from the TEDS are 111 then the flow chart for parsing transducer TEDS calls for
// 7-bit ASCII User Data to fill the rest of the EEPROM.
ENDTEMPLATE
 
//-------------------------------------------------------------------------------------------

TEMPLATE 0,8,1,"Accelerometer with transfer function v0.9"  
TDL_VERSION_NUMBER 2  //Version 2 refers to the final IEEE 1451.4 version 1.0 TDL specification
ABSTRACT IEEE P1451.4 template ID 1, version 0.9
ABSTRACT ROM not used.
ABSTRACT Updated to TDL version 2 draft
SPACING

//Physical Base Units: (ratio, radian, steradian, meter, kg, sec, Amp, K, mole, candela)
PHYSICAL_UNIT "V/(m/s^2)",  (0,0,0,1,1,-1,-1,0,0,0,1,0)        // equals m*kg/(s*A)
PHYSICAL_UNIT "Hz",        (0,0,0,0,0,-1,0,0,0,0,1,0)         // equals 1/s
PHYSICAL_UNIT "degrees",   (0,1,0,0,0,0,0,0,0,0,0.0174533,0)  // degrees = 0.0174533 radians
PHYSICAL_UNIT "%/decade",  (6,0,0,0,0,-1,0,0,0,0,0.01,0)      // Flatness over frequency: 0.01 / (log10(Hz/Hz))
PHYSICAL_UNIT "%/Deg C",      (0,0,0,0,0,0,0,-1,0,0,0.01,0)      // Temperature drift: %/Deg C = 0.01 x (1/Kelvin)
PHYSICAL_UNIT "",          (0,0,0,0,0,0,0,0,0,0,1,0)          // dimensionless

%CalDate, "Calibration date", CAL, 16, DATE,"d-mmm-yyyy",""
%Sens@Ref, "Sensitivity @ Fref", CAL, 16, ConRelRes, 100E-6, 0.0001,  "0.000p", "V/(m/s^2)"
%Reffreq,  "Fref", CAL, 8, ConRelRes, 10.17501895022, 0.015,  "0p","Hz"
%TF_HP_S, "F hp electrical", CAL, 12, ConRelRes, 0.01, 0.001, "0.000p", "Hz"
%Sign,  "Phase invertion (0: 0 Deg, 1: 180 Deg)", CAL, 1, UNINT, "0",""
%TF_SP, "Low pass cut-off frequency (F lp)", CAL, 12, ConRelRes, 100, 0.0015, "rp", "Hz"
%TF_KPr, "Resonance frequency (F res)", CAL, 9, ConRelRes, 100, 0.01, "rp", "Hz"
%TF_KPq, "Quality factor @ F res (Q)", CAL, 8, ConRelRes, 0.3, 0.03,  "rp",""
%TF_SL, "Amplitude slope (a)", CAL, 7, ConRelRes, 0.852279961333371, 0.001, "0.00p", "%/decade"  
%PhaseCorrection, "Phase correction @ reference", CAL, 6, CONRES, -3.2, 0.1, "rp", "degrees"
%TempCoef, "Temperature coefficient (b)", CAL, 9, ConRelRes, 1E-6, 0.01,"0.00p","%/Deg C"

ENUMERATE DirectionEnum,"x","y","z"
%Direction, "Sensitivity direction (x,y,z)",  CAL, 2, DirectionEnum, "e", ""
SPACING

%MeasID, "Measurement location ID",  USR, 9, UNINT,"###0", ""

ALIGN 8  //May not be needed, but left for compatibility with previous version

ENUMERATE ElecSigTypeEnum,"Voltage Sensor","Current Sensor","Resistance Sensor","Bridge Sensor","LVDT Sensor","Potentiometric Voltage Divider Sensor","Pulse Sensor","Voltage Actuator","Current Actuator","Pulse Actuator"
%ElecSigType, "Transducer Electrical Signal Type", ID, 0, ElecSigTypeEnum,"e","" = "Voltage Sensor"

ENUMERATE MapMethEnum, "Linear","Inverse m/(x+b)","Inverse (b+m/x)","Thermocouple","Thermistor","RTD","Bridge"
%MapMeth, "Mapping Method", ID, 0, MapMethEnum,"e","" = "Linear"

ENUMERATE ACDCCouplingEnum,"DC","AC"
%ACDCCoupling, "AC or DC Coupling", ID, 0, ACDCCouplingEnum,"e","" = "AC"

//User data is not explicitly defined in this subtemplate. If the next three bits read
// from the TEDS are 111 then the flow chart for parsing transducer TEDS calls for
// 7-bit ASCII User Data to fill the rest of the EEPROM.
ENDTEMPLATE
 
//-------------------------------------------------------------------------------------------

TEMPLATE 0,8,12,"Microphone v0.9"  
TDL_VERSION_NUMBER 2  //Version 2 refers to the final IEEE 1451.4 version 1.0 TDL specification
ABSTRACT IEEE P1451.4 template ID 12, version 0.9
ABSTRACT ROM not used.
ABSTRACT Updated to TDL version 2 draft
SPACING

//Physical Base Units: (ratio, radian, steradian, meter, kg, sec, Amp, K, mole, candela)
PHYSICAL_UNIT "V/Pa",      (0,0,0,3,0,-1,-1,0,0,0,1,0)    // Volts/Pascal equals m^3/(sec*A)
PHYSICAL_UNIT "",          (0,0,0,0,0,0,0,0,0,0,1,0)      // dimensionless

%CalDate, "Calibration date", CAL, 16, DATE,"d-mmm-yyyy",""
%Sens@Ref, "Sensitivity @ 250 Hz", CAL, 16, ConRelRes, 100E-6, 0.0001, "rp", "V/Pa"
%Prepolarized, "Prepolarized  (0/1)", CAL, 1, UNINT,"",""
SPACING
%MeasID, "Measurement location ID",  USR, 9, UNINT,"###0", ""

ALIGN 8  //May not be needed, but left for compatibility with previous version

ENUMERATE ElecSigTypeEnum,"Voltage Sensor","Current Sensor","Resistance Sensor","Bridge Sensor","LVDT Sensor","Potentiometric Voltage Divider Sensor","Pulse Sensor","Voltage Actuator","Current Actuator","Pulse Actuator"
%ElecSigType, "Transducer Electrical Signal Type", ID, 0, ElecSigTypeEnum,"e","" = "Voltage Sensor"

ENUMERATE MapMethEnum, "Linear","Inverse m/(x+b)","Inverse (b+m/x)","Thermocouple","Thermistor","RTD","Bridge"
%MapMeth, "Mapping Method", ID, 0, MapMethEnum,"e","" = "Linear"

ENUMERATE ACDCCouplingEnum,"DC","AC"
%ACDCCoupling, "AC or DC Coupling", ID, 0, ACDCCouplingEnum,"e","" = "AC"

//User data is not explicitly defined in this subtemplate. If the next three bits read
// from the TEDS are 111 then the flow chart for parsing transducer TEDS calls for
// 7-bit ASCII User Data to fill the rest of the EEPROM.
ENDTEMPLATE
 
//-------------------------------------------------------------------------------------------

TEMPLATE 0,8,24,"Accelerometer, transfer function v0.91"  
TDL_VERSION_NUMBER 2  //Version 2 refers to the final IEEE 1451.4 version 1.0 TDL specification
ABSTRACT IEEE P1451.4 template version 0.91
ABSTRACT ROM not used.
ABSTRACT Updated to TDL version 2 draft
SPACING

//Physical Base Units: (ratio, radian, steradian, meter, kg, sec, Amp, K, mole, candela)
PHYSICAL_UNIT "V/(m/s^2)",  (0,0,0,1,1,-1,-1,0,0,0,1,0)       // equals m*kg/(s*A)
PHYSICAL_UNIT "Hz",        (0,0,0,0,0,-1,0,0,0,0,1,0)        // equals 1/s
PHYSICAL_UNIT "%/Deg C",      (0,0,0,0,0,0,0,-1,0,0,0.01,0)     // Temperature drift: %/Deg C = 0.01 x (1/Kelvin)
PHYSICAL_UNIT "Deg C",        (0,0,0,0,0,0,0,1,0,0,1,-273.15)   // Celsius is (kelvin - 273.15 K)
PHYSICAL_UNIT "%/decade",  (6,0,0,0,0,-1,0,0,0,0,0.01,0)     // Flatness over frequency: 0.01 / (log10(Hz/Hz))
PHYSICAL_UNIT "",          (0,0,0,0,0,0,0,0,0,0,1,0)         // dimensionless

%CalDate, "Calibration date", CAL, 16, DATE,"d-mmm-yyyy",""
%Sens@Ref, "Sensitivity @ ref. cond. (S ref)",  CAL, 16, ConRelRes, 5E-7, 0.00015, "rp", "V/(m/s^2)"
%Reffreq, "Reference frequency (f ref)",  CAL, 8, ConRelRes, 7.991035024961, 0.014735074616, "rp","Hz"
ENUMERATE SignEnum,"Positive","Negative"
%Sign, "Polarity (Sign)", CAL, 1, SignEnum, "e", ""

%TF_HP_S, "High pass cut-off frequency (f hp)", CAL, 12, ConRelRes, 0.01, 0.0015, "rp", "Hz"
%TF_SP, "Low pass cut-off frequency (f lp)",  CAL, 12, ConRelRes, 20, 0.0015, "rp", "Hz"
%TF_KPr, "Resonance frequency (f res)",  CAL, 9, ConRelRes, 100, 0.01, "rp", "Hz"
%TF_KPq, "Quality factor @ f res (Q)",  CAL, 7, ConRelRes, 0.3, 0.03,  "rp",""
%TF_SL, "Amplitude slope (a)",  CAL, 7, ConRes, -6.3, 0.1, "0.0", "%/decade"  
%TempCoef, "Temperature coefficient (b)",  CAL, 9, ConRes, -0.5, 0.002,"0.000","%/Deg C"
%RefTemp, "Reference temperature (T ref)",  CAL, 5, ConRes, 15, 0.5, "0.0","Deg C"  
 
ENUMERATE DirectionEnum,"x","y","z"
%Direction, "Sensitivity direction (x,y,z)",  CAL, 2, DirectionEnum, "e", ""
SPACING

%MeasID, "Meas. position ID",  USR, 11, UNINT,"###0", ""

ENUMERATE ElecSigTypeEnum,"Voltage Sensor","Current Sensor","Resistance Sensor","Bridge Sensor","LVDT Sensor","Potentiometric Voltage Divider Sensor","Pulse Sensor","Voltage Actuator","Current Actuator","Pulse Actuator"
%ElecSigType, "Transducer Electrical Signal Type", ID, 0, ElecSigTypeEnum,"e","" = "Voltage Sensor"

ENUMERATE MapMethEnum, "Linear","Inverse m/(x+b)","Inverse (b+m/x)","Thermocouple","Thermistor","RTD","Bridge"
%MapMeth, "Mapping Method", ID, 0, MapMethEnum,"e","" = "Linear"

ENUMERATE ACDCCouplingEnum,"DC","AC"
%ACDCCoupling, "AC or DC Coupling", ID, 0, ACDCCouplingEnum,"e","" = "AC"

//User data is not explicitly defined in this subtemplate. If the next three bits read
// from the TEDS are 111 then the flow chart for parsing transducer TEDS calls for
// 7-bit ASCII User Data to fill the rest of the EEPROM.
ENDTEMPLATE

//-------------------------------------------------------------------------------------------

Validation_Keycode 12345
